package edu.sjsu.android.googlemaps2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class LocationsDB extends SQLiteOpenHelper {
    public static final String DATABASE_TABLE = "Markers";
    public static final String ID_COLUMN = "id";
    public static final String LATITUDE_COLUMN = "latitude";
    public static final String LONGITUDE_COLUMN = "longitude";
    public static final String ZOOM_COLUMN = "zoom_level";
    public static final int DATABASE_VERSION = 1;
    private static final String DATABASE_CREATE = String.format(
            "CREATE TABLE %s (" +
                    " %s integer primary key autoincrement, " +
                    " %s double," +
                    " %s double," +
                    " %s real)",
            DATABASE_TABLE, ID_COLUMN, LATITUDE_COLUMN, LONGITUDE_COLUMN, ZOOM_COLUMN
    );
    private SQLiteDatabase db;
    public LocationsDB(Context context) {
        super(context, DATABASE_TABLE, null, DATABASE_VERSION);
        db = getWritableDatabase();
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
        onCreate(sqLiteDatabase);
    }

    public long insert( double latitude, double longitude, float zoom){
        ContentValues newValues = new ContentValues();
        newValues.put(LocationsDB.LATITUDE_COLUMN, latitude);
        newValues.put(LocationsDB.LONGITUDE_COLUMN, longitude);
        newValues.put(LocationsDB.ZOOM_COLUMN, zoom);
        return db.insert(LocationsDB.DATABASE_TABLE, null, newValues);
    }
    public long insert(ContentValues newValues){
        return db.insert(LocationsDB.DATABASE_TABLE, null, newValues);
    }

    public int deleteAll(){
        return db.delete(LocationsDB.DATABASE_TABLE, null, null);
    }

    public Cursor returnAll(){
        return db.query(LocationsDB.DATABASE_TABLE, null, null, null, null, null, null);
    }
}
