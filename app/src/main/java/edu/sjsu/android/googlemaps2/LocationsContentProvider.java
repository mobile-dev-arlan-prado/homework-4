package edu.sjsu.android.googlemaps2;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.URL;

public class LocationsContentProvider extends ContentProvider {

    public static final String PROVIDER_NAME = "edu.sjsu.android.googlemaps2.locationsprovider";
    public static final String URL = "content://" + PROVIDER_NAME + "/locationsprovider";
    public static final Uri CONTENT_URI = Uri.parse(URL);
    public static final int LOCATIONS = 1;
    public static final UriMatcher uriMatcher;
    private LocationsDB locationsDB;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "locationsprovider", LOCATIONS);
    }
    @Override
    public boolean onCreate() {
        locationsDB = new LocationsDB(getContext());
        return (locationsDB == null) ? false : true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        if(uriMatcher.match(uri) == LOCATIONS)
            return locationsDB.returnAll();
        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        long id = locationsDB.insert(contentValues);
        if(id > 0){
            Uri uri1 = ContentUris.withAppendedId(CONTENT_URI, id);
            getContext().getContentResolver().notifyChange(uri1, null);
            return uri;
        }
        throw new android.database.SQLException("Failed to add record " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return locationsDB.deleteAll();
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
